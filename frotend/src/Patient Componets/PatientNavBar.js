import React from "react"
import {Navbar,Nav} from "react-bootstrap"

function PatientNavBar(){
    return(
        <Navbar expend="lg" bg="dark" variant="dark">
            <Nav className="mr-auto">
                <Nav.Link href="/patient">Home</Nav.Link>
                <Nav.Link href="/patientPlan">Medical Plan</Nav.Link>
            </Nav>
            <Nav className="justify-content-end">
                <Nav.Link href="/">Log out</Nav.Link>
            </Nav>
        </Navbar>    
    )
}

export default PatientNavBar