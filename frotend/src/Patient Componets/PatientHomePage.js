import React from "react"
import {Form,FormGroup,Input,Label} from "reactstrap"
import axios from "axios"
import PatientNavBar from "./PatientNavBar"

class PatientHomePage extends React.Component{
    constructor(){
        super()
        this.state={
            username:"",
            password:"",
            name:"",
            address:"",
            gender:"",
            type:"",
            birthDate:"",
            caregiver:{},
            idPatient:"",
            idUser:localStorage.getItem("iduser"),
        }
    }

    componentDidMount(){
        console.log(this.state.idUser)
        axios.get(`http://localhost:8090/patient/getPatientByUserId/${this.state.idUser}`)
        .then(response=>{
            this.setState({
                idPatient:response.data.idPatient,
                username:response.data.user.username,
                password:response.data.user.password,
                type:response.data.user.type,
                name:response.data.name,
                address:response.data.address,
                gender:response.data.gender,
                birthDate:response.data.birthDate,
                caregiver:response.data.caregiver,

            })
            localStorage.setItem("idPatient",this.state.idPatient)
        })
    }
    render(){
        return(
            <div>
                <PatientNavBar/>
                <Form className="add-form">

                <h3>
                        <span>
                        Patient Account Details
                        </span>
                    </h3>
                    <FormGroup>
                        <Label className="add-label">Username</Label>
                        <Input type="text"
                        value={this.state.username}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label className="add-label">Password</Label>
                        <Input type="text"
                        value={this.state.password}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label className="add-label">Name</Label>
                        <Input type="text"
                        value={this.state.name}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label className="add-label">Address</Label>
                        <Input type="text"
                        value={this.state.address}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label className="add-label">Gender</Label>
                        <Input type="text"
                        value={this.state.gender}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label className="add-label">Birth Date</Label>
                        <Input type="text"
                        value={this.state.birthDate}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label className="add-label">Caregiver Name</Label>
                        <Input type="text"
                        value={this.state.caregiver.name}
                        />
                    </FormGroup>
                </Form>
            </div>
        )
    }
}

export default PatientHomePage