import React from "react"
import axios from "axios"
import PatientNavBar from "./PatientNavBar"
import ReactTable from "react-table"
import "react-table/react-table.css"


class ViewPlan extends React.Component{
    constructor(){
        super()
        this.state={
            idPatient:localStorage.getItem("idPatient"),
        }
        this.getPlan=this.getPlan.bind(this)
    }

    componentDidMount(){
        this.getPlan()
    }

    getPlan(){
        axios.get(`http://localhost:8090/intake/getPatientIntake/${this.state.idPatient}`)
        .then(response=>{
            this.setState({
                plan:response.data
            })
            console.log(this.state.plan)
        })
    }

    render(){
        return(
            <div>
                <PatientNavBar/>
                <ReactTable
                defaultPageSize={10}
                data={this.state.plan}
                columns={[
                    {
                        Header:"Medication Name",
                        accessor:"medication.name"

                    },
                    {
                        Header:"Dosage",
                        accessor:"medication.dosage"
                    },
                    {
                        Header:"Side Effects",
                        accessor:"medication.sideEffects"
                    },
                    {
                        Header:"Start time",
                        accessor:"startTime"
                    },
                    {
                        Header:"End time",
                        accessor:"endTime"
                    }
                ]}
               />
            </div>
        )
    }
}

export default ViewPlan