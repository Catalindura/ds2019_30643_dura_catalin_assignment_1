import React from "react"
import axios from "axios"
import CaregiverNavBar from "./CaregiverNavBar"
import ReactTable from "react-table"
import "react-table/react-table.css"


class ViewPatients extends React.Component{
    constructor(){
        super()
        this.state={
            idUser:localStorage.getItem("iduser"),
            patients:[],
        }
        this.getPatients=this.getPatients.bind(this)
    }

    componentDidMount(){
        this.getPatients()
    }

    getPatients(){
        axios.get(`http://localhost:8090/caregiver/getCaregiverList/${this.state.idUser}`)
        .then(response=>{
            this.setState({
                patients:response.data
            })
            console.log(this.state.patients)
        })
    }

    render(){
        return(
            <div>
                <CaregiverNavBar/>
                <ReactTable
                defaultPageSize={10}
                data={this.state.patients}
                columns={[
                    {
                        Header:"Name",
                        accessor:"name"

                    },
                    {
                        Header:"Address",
                        accessor:"address"
                    },
                    {
                        Header:"Birth Date",
                        accessor:"birthDate"
                    },
                    {
                        Header:"Gender",
                        accessor:"gender"
                    },
                ]}
               />
            </div>
        )
    }
}

export default ViewPatients