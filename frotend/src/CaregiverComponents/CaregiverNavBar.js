import React from "react"
import {Navbar,Nav} from "react-bootstrap"

function CaregiverNavBar(){
    return(
        <Navbar expend="lg" bg="dark" variant="dark">
            <Nav className="mr-auto">
                <Nav.Link href="/caregiver">Home</Nav.Link>
                <Nav.Link href="/viewPatients">View Patients</Nav.Link>
            </Nav>
            <Nav className="justify-content-end">
                <Nav.Link href="/">Log out</Nav.Link>
            </Nav>
        </Navbar>    
    )
}

export default CaregiverNavBar