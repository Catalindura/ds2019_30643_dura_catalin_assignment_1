import React from "react"
import CaregiverNavBar from "./CaregiverNavBar"
import axios from "axios"
class CaregiverHomePage extends React.Component{
    constructor(){
        super()
    }
    render(){
        return(
            <div>
                <CaregiverNavBar/>
                <h1>
                Caregiver Home Page!
                </h1>
            </div>
        )
    }
}

export default CaregiverHomePage