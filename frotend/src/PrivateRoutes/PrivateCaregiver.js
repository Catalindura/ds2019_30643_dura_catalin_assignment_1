import React from "react"
import {Redirect,Route} from "react-router-dom"

export const PrivateRouteCaregiver=({component:Component,...rest})=>(
    <Route
    {...rest}
    render={props=>
    localStorage.getItem("auth")==="caregiver"?(

        <Component {...props}/>
    ):(
        <Redirect
        to={{
            pathname:"/",
            state:{from:props.location}

        }}
        />
    )
    }
    />
);