import React from "react"
import {Redirect,Route} from "react-router-dom"

export const PrivateRoutePatient=({component:Component,...rest})=>(
    <Route
    {...rest}
    render={props=>
    localStorage.getItem("auth")==="pacient"?(

        <Component {...props}/>
    ):(
        <Redirect
        to={{
            pathname:"/",
            state:{from:props.location}

        }}
        />
    )
    }
    />
);