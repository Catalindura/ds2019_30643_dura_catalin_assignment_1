import React from "react"
import {Redirect,Route} from "react-router-dom"

export const PrivateRouteMedic=({component:Component,...rest})=>(
    <Route
    {...rest}
    render={props=>
    localStorage.getItem("auth")==="medic"?(

        <Component {...props}/>
    ):(
        <Redirect
        to={{
            pathname:"/",
            state:{from:props.location}

        }}
        />
    )
    }
    />
);