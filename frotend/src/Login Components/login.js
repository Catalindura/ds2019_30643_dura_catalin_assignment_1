import React from "react"
import {Form,FormGroup,Input,Label,Button} from "reactstrap"
import { FaUser,FaLock } from "react-icons/fa";
import axios from "axios"

class LogIn extends React.Component{
    constructor(){
        super()
        this.state={
            username:"",
            password:"",
            error:"false",
        }
        this.handleChange=this.handleChange.bind(this)
        this.handleClick=this.handleClick.bind(this)
    }

    componentDidMount(){
        localStorage.setItem("auth","none")
    }

    handleClick(){
        axios.get(`http://localhost:8090/user/getUser/${this.state.username}`)
        .then(response=>{
            console.log(response.data)
            localStorage.setItem("iduser",response.data.idUser)
            if(response.data.password===this.state.password)
            {
                localStorage.setItem("auth",response.data.type)
                if(response.data.type==="medic") {
                    this.props.history.push("/medic")
                }
                else if(response.data.type==="pacient"){
                    this.props.history.push("/patient")
                }
                    else {
                        this.props.history.push("/caregiver")
                    }
            }
            else this.setState({
                error:"true"
            })     
        })
    }

    handleChange(event){
        const {name,value}=event.target
        this.setState({
            [name]:value
        })        
        console.log(this.state.error)
    }
    render(){
        return(
            <div className="body">
                <Form className="login-form">
                    <span>
                        <h3>Sign up</h3>
                    </span>
                    <FormGroup>
                        <FaUser/>
                        <Label className="login-label">Username</Label>
                        <Input className="input" type="text" 
                        name="username"
                        onChange={this.handleChange}
                        value={this.state.username}
                        placeholder="Username"
                        />
                    </FormGroup>
                    <FormGroup>
                        <FaLock/>
                        <Label className="login-label">Password</Label>
                        <Input className="input" type="password"
                        placeholder="Password"
                        onChange={this.handleChange}
                        value={this.state.password}
                        name="password"
                        />
                    </FormGroup>
                    <FormGroup>
                    { this.state.error==="true" ? <p><b>Username or password incorrect!!</b></p>
                    :<p></p>}
                </FormGroup>
                    <Button className="btn-lg btn-dark btn-block" 
                    onClick={this.handleClick}>Log In</Button>
                </Form>
            </div>
        )
    }
}

export default LogIn