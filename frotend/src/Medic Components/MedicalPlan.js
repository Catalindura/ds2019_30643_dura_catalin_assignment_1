import React from "react"
import axios from "axios"
import MedicNavBar from "./MedicNavBar"
import ChoseMedication from "./Medication/ChoseMedication"

class MedicalPlan extends React.Component{
    constructor(){
        super()
        this.state={
            patients:[],
            medication:"false",
            patient:{}
        }

        this.handlePlan=this.handlePlan.bind(this)
    }
    componentDidMount(){
        this.getUsers()
    }

    getUsers(){
        axios.get("http://localhost:8090/patient/getAllPatients")
        .then(response=>{
            this.setState({
                patients:response.data
            })
            console.log(this.state.patients)
        })
    }

    handlePlan(value){
        console.log("Da")
        axios.get(`http://localhost:8090/patient/getPatientById/${value}`)
        .then(response=>{
        this.setState({
            medication:true,
            patient:response.data
        })
        console.log(this.state.patient)
    })

    }

    render(){
        return(
            <div>
                <MedicNavBar/>
                {this.state.medication==="false" ?
                <table>
                    <tbody>
                        <tr>
                            <td>Username</td>
                            <td>Password</td>
                            <td>Name</td>
                            <td>Gender</td>
                            <td>Birth date</td>
                            <td>Address</td>
                            <td>Caregiver Name</td>
                            <td>Medical Plan</td>
                        </tr>
                        {this.state.patients.map(user=>
                            <tr>
                                <td>{user.user.username}</td>
                                <td>{user.user.password}</td>
                                <td>{user.name}</td>
                                <td>{user.gender}</td>
                                <td>{user.birthDate}</td>
                                <td>{user.address}</td>
                                <td>{user.caregiver.name}</td>
                                <td><button className="button3" onClick={this.handlePlan.bind(this,user.idPatient)}>+</button></td>
                            </tr>
                            )}
                    </tbody>
                </table>
                :
                <ChoseMedication
                patient={this.state.patient}/>}          
            </div>
        )
    }
}

export default MedicalPlan