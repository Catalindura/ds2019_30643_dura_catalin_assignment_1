import React from "react"
import {Form,FormGroup,Input,Label,Button} from "reactstrap"
import axios from "axios"
import MedicNavBar from "./MedicNavBar"

class CreateUser extends React.Component{
    constructor(){
        super()
        this.state={
            userExists:"false",
            currentUser:{},
            username:"",
            name:"",
            password:"",
            gender:"",
            address:"",
            birthdate:"",
            day:"",
            month:"",
            year:"",
            type:"",
            doctor:{},
            iddoctor:localStorage.getItem("iduser"),
            iduser:0,
            firstCaregiver:{}
        }
        this.handleChange=this.handleChange.bind(this)
        this.handleClick=this.handleClick.bind(this)
    }

    componentDidMount(){
        axios.get(`http://localhost:8090/medic/getDoctorByUserId/${this.state.iddoctor}`)
        .then(response=>{
            this.setState({
                doctor:response.data
            })
            console.log(this.state.doctor)
        })
        axios.get(`http://localhost:8090/caregiver/getFirstCaregiver`)
        .then(response=>{
            this.setState({
                firstCaregiver:response.data
            })
            console.log(this.state.firstCaregiver)
        })
    }

    handleChange(event){
        console.log(this.state.gender)
        const {name,value}=event.target
        this.setState({
            [name]:value
        })
    }

    handleClick(){
       const account={
           username:this.state.username,
           password:this.state.password,
           type:this.state.type,
       }

       axios.get(`http://localhost:8090/user/checkUser/${account.username}`)
       .then(response=>{

        if(response.data==="no") this.setState({
            userExists:"true"
        })
        else {
       axios.post("http://localhost:8090/user/saveUser",account)
       .then(response=>{
           this.setState({
               iduser:response.data
           })
           const account={
               idUser:this.state.iduser,
               username:this.state.username,
               password:this.state.password,
               type:this.state.type,
           }
           const user={
            name:this.state.name,
            address:this.state.address,
            birthDate:this.state.day+"/"+this.state.month+"/"+this.state.year,
            user:account,
            doctor:this.state.doctor,
            gender:this.state.gender,
            caregiver:this.state.firstCaregiver,
           }
           if(account.type==="caregiver"){
                delete user.caregiver
                axios.post("http://localhost:8090/caregiver/saveCaregiver",user)
           }
           else {
                delete user.doctor
                axios.post("http://localhost:8090/patient/savePatient",user)
           }

        })}
    })
    }

    render(){
        return(
            <div className="body">
                <MedicNavBar/>
                <Form className="add-form">

                    <h3>
                        <span className="user-message">
                        Registration
                        </span>
                    </h3>

                    <FormGroup>
                        <Label className="add-label">Username</Label>
                        <Input className="input" type="text"
                        value={this.state.username}
                        name="username"
                        placeholder="username"
                        onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label className="add-label">Password</Label>
                        <Input className="input" type="text"
                        value={this.state.password}
                        name="password"
                        placeholder="Password"
                        onChange={this.handleChange}
                        />
                    </FormGroup>

                    
                    <FormGroup>
                        <Label className="add-label">Name</Label>
                        <Input className="input" type="text"
                        value={this.state.name}
                        name="name"
                        placeholder="Name"
                        onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label className="add-label">Address</Label>
                        <Input className="input" type="text"
                        value={this.state.address}
                        name="address"
                        placeholder="Address"
                        onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup check inline>
                    <FormGroup>
                        <Label className="add-label">Gender</Label>
                    <FormGroup>
                    <Label className="radio-label">
                        <Input className="input-add" type="radio"
                        name="gender"
                        value="male"
                        checked={this.state.gender==="male"}
                        onChange={this.handleChange}/>
                        Male</Label>
                        <Label className="radio-label">
                        <Input className="input-add" type="radio"
                        name="gender"
                        value="female"
                        checked={this.state.gender==="female"}
                        onChange={this.handleChange}/>
                        Female</Label>
                    </FormGroup>
                    </FormGroup>

                    <FormGroup>
                    <Label className="add-label">Type</Label>
                    <FormGroup>
                    <Label className="radio-label"> 
                        <Input className="input-add" type="radio"
                        name="type"
                        value="pacient"
                        checked={this.state.type==="pacient"}
                        onChange={this.handleChange}/>
                        Pacient</Label>
                        <Label className="radio-label">
                        <Input className="input-add" type="radio"
                        name="type"
                        value="caregiver"
                        checked={this.state.type==="caregiver"}
                        onChange={this.handleChange}/>
                        Caregiver</Label>
                    </FormGroup>
                    </FormGroup>

                    </FormGroup>

                    <FormGroup>
                        <Label className="add-label">Data of Birth</Label>
                        <FormGroup check inline>
                        <Input className="date-input"
                        type="text" 
                        name="day"
                        value={this.state.day}
                        onChange={this.handleChange}
                        placeholder="DD"
                        />
                          <Input className="date-input"
                        type="text" 
                        name="month"
                        value={this.state.month}
                        onChange={this.handleChange}
                        placeholder="MM"
                        />
                    <Input 
                        className="date-input"
                        type="text" 
                        name="year"
                        value={this.state.year}
                        onChange={this.handleChange}
                        placeholder="YYYY"
                        />
                    </FormGroup>
                    </FormGroup>
                    <FormGroup>
                        {this.state.userExists==="true" ?<p>Username already exists</p>:<h3></h3>} 
                    </FormGroup>
                    <FormGroup>
                    <Button className="btn-lg btn-block btn-succes" onClick={this.handleClick}>Submit</Button>
                    </FormGroup>
                </Form>
            </div>
        )
    }
}

export default CreateUser;