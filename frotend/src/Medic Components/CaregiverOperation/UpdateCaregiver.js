import React from "react"
import {Form,FormGroup,Input,Label,Button} from "reactstrap"
import axios from "axios"

class UpdateUser extends React.Component{
    constructor(props){
        super()
        this.state={
            username:props.username,
            name:props.name,
            password:props.password,
            gender:props.gender,
            address:props.address,
            birthDate:props.birthDate,
            doctor:props.iddoctor,
            idAccount:props.idAccount,
            type:props.type,
            idCaregiver:props.idCaregiver
        }
        this.handleChange=this.handleChange.bind(this)
        this.handleClick=this.handleClick.bind(this)
    }

    handleChange(event){
        const{name,value}=event.target
        this.setState({
            [name]:value
        })
    }

    handleClick(){
        const account={
            username:this.state.username,
            password:this.state.password,
            type:this.state.type,
            idUser:this.state.idAccount
        }

        axios.post("http://localhost:8090/user/saveUser",account)
        .then(response=>{
        const patient={
            idPatient:this.state.idPatient,
            name:this.state.name,
            gender:this.state.gender,
            address:this.state.address,
            birthDate:this.state.birthDate,
            user:account,
            caregiver:this.state.caregiver,
        }
        axios.post("http://localhost:8090/patient/savePatient",patient)
        .then(response=>
            console.log(response))
        .catch(error=>
            console.log(error.response))
        })
    }


    render(){
        return(
            <div>
                <Form className="add-form"> 
                <h3>
                        <span className="update-message">
                        Update {this.state.name}`s account
                        </span>
                    </h3>

                    <FormGroup>
                        <Label className="login-label">Username</Label>
                        <Input type="text"
                        name="username"
                        value={this.state.username}
                        onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label className="login-label">Password</Label>
                        <Input type="text"
                        name="password"
                        value={this.state.password}
                        onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label className="login-label">Name</Label>
                        <Input type="text"
                        name="name"
                        value={this.state.name}
                        onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label className="login-label">Gender</Label>
                        <Input type="text"
                        name="gender"
                        value={this.state.gender}
                        onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label className="login-label">Address</Label>
                        <Input type="text"
                        name="address"
                        value={this.state.address}
                        onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label className="login-label">Birth Date</Label>
                        <Input type="text"
                        name="birthDate"
                        value={this.state.birthDate}
                        onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup>
                    <Button className="btn-lg btn-block btn-succes" onClick={this.handleClick}>Update</Button>
                    </FormGroup>

                </Form>
                
            </div>
        )
    }
}

export default UpdateUser