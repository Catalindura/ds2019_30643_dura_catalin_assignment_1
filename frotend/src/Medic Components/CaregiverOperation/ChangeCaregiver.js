import React from "react"
import axios from "axios"
import MedicNavBar from "../MedicNavBar"
import UpdateUser from "../PatientOperation/UpdateUser"

class ChangeCaregiver extends React.Component{
    constructor(props){
        super()
        console.log(props.caregiver)
        console.log(props.patient)
        this.state={
            caregivers:[],
            idPatient:localStorage.getItem("idPatient"),
            patient:{},
        }
        this.getCaregivers=this.getCaregivers.bind(this)
        this.handleChange=this.handleChange.bind(this)
    }
    componentDidMount(){
        this.getCaregivers()
        this.getUser()
    }

    getUser(){
        axios.get(`http://localhost:8090/patient/getPatientById/${this.state.idPatient}`)
        .then(response=>{
            this.setState({
                patient:response.data,
            })
            console.log(this.state.patient)
        }
            )
    }
    getCaregivers(){
        axios.get("http://localhost:8090/caregiver/getAllCaregivers")
        .then(response=>{
            this.setState({
                caregivers:response.data
            })
            console.log(this.state.caregivers)
        })
    }

    handleChange(value){
        axios.get(`http://localhost:8090/caregiver/getCaregiverById/${value}`)
        .then(response=>{
            const patient={
                idPatient:this.state.patient.idPatient,
                name:this.state.patient.name,
                gender:this.state.patient.gender,
                address:this.state.patient.address,
                birthDate:this.state.patient.birthDate,
                user:this.state.patient.user,
                caregiver:response.data
                        }
            console.log(patient)
            axios.post("http://localhost:8090/patient/savePatient",patient)
            .then(response=>
                console.log(response))
                    })

    }

    render(){
        return(
            <div>
                <MedicNavBar/>
                <table>
                    <tbody>
                        <tr>
                            <td>Username</td>
                            <td>Name</td>
                            <td>Gender</td>
                            <td>Birth date</td>
                            <td>Address</td>
                            <td>Doctor Name</td>
                            <td>Change</td>
                        </tr>
                        {this.state.caregivers.map(user=>
                            <tr>
                                <td>{user.user.username}</td>
                                <td>{user.name}</td>
                                <td>{user.gender}</td>
                                <td>{user.birthDate}</td>
                                <td>{user.address}</td>
                                <td>{user.doctor.name}</td>
                                <td><button className="button3" onClick={this.handleChange.bind(this,user.idCaregiver)}>Change</button></td>
                            </tr>
                            )}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ChangeCaregiver