import React from "react"
import axios from "axios"
import MedicNavBar from "../MedicNavBar"
import UpdateUser from "../PatientOperation/UpdateUser"

class CaregiverList extends React.Component{
    constructor(){
        super()
        this.state={
            caregivers:[],
            update:"false",
            caregiverToBeUpdated:{},
        }
        this.getCaregivers=this.getCaregivers.bind(this)
        this.handleDelete=this.handleDelete.bind(this)
    }
    componentDidMount(){
        this.getCaregivers()
    }

    getCaregivers(){
        axios.get("http://localhost:8090/caregiver/getAllCaregivers")
        .then(response=>{
            this.setState({
                caregivers:response.data
            })
            console.log(this.state.caregivers)
        })
    }

    handleDelete(value){
        console.log(value)
        axios.post(`http://localhost:8090/caregiver/deleteCaregiver/${value}`)
        .then(()=>this.getCaregivers())
        .catch(error=>
            console.log(error))
    }

    handleUpdate(value){
        axios.get(`http://localhost:8090/caregiver/getCaregiverById/${value}`)
        .then(response=>{
            this.setState({
                userToBeUpdated:response.data,
                update:"true",
            })
            console.log(this.state.userToBeUpdated)
        }
            )
    }

    render(){
        return(
            <div>
                <MedicNavBar/>
                {this.state.update==="false" ?
                <table>
                    <tbody>
                        <tr>
                            <td>Username</td>
                            <td>Password</td>
                            <td>Name</td>
                            <td>Gender</td>
                            <td>Birth date</td>
                            <td>Address</td>
                            <td>Doctor Name</td>
                            <td>Delete</td>
                            <td>Update</td>
                        </tr>
                        {this.state.caregivers.map(user=>
                            <tr>
                                <td>{user.user.username}</td>
                                <td>{user.user.password}</td>
                                <td>{user.name}</td>
                                <td>{user.gender}</td>
                                <td>{user.birthDate}</td>
                                <td>{user.address}</td>
                                <td>{user.doctor.name}</td>
                                <td><button className="button2" onClick={this.handleDelete.bind(this,user.idCaregiver)}>X</button></td>
                                <td><button className="button3" onClick={this.handleUpdate.bind(this,user.idCaregiver)}>Update</button></td>
                            </tr>
                            )}
                    </tbody>
                </table>
                :
                <UpdateUser
                username={this.state.userToBeUpdated.user.username}
                type={this.state.userToBeUpdated.user.type}
                idAccount={this.state.userToBeUpdated.user.idUser}
                name={this.state.userToBeUpdated.name}
                password={this.state.userToBeUpdated.user.password}
                gender={this.state.userToBeUpdated.gender}
                birthDate={this.state.userToBeUpdated.birthDate}
                address={this.state.userToBeUpdated.address}
                idPatient={this.state.userToBeUpdated.idCaregiver}
                doctor={this.state.userToBeUpdated.doctor}
                idCaregiver={this.state.userToBeUpdated.idCaregiver}/>
                        }                        
            </div>
        )
    }
}

export default CaregiverList