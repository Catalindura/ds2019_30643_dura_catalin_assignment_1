import React from "react"
import {Navbar,NavDropdown,Nav} from "react-bootstrap"

function MedicNavBar(){
    return(
        <Navbar expend="lg" bg="dark" variant="dark">
            <Nav className="mr-auto">
                <Nav.Link href="/medic">Home</Nav.Link>
                <NavDropdown title="User Operations">
                    <NavDropdown.Item href="/createUser">Add User</NavDropdown.Item>
                    <NavDropdown.Item href="/caregiverList">Update/Delete Caregiver</NavDropdown.Item>
                    <NavDropdown.Item href="/patientList">Update/Delete User</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title="Medication Operations">
                    <NavDropdown.Item href="/createMedication">Add Medication</NavDropdown.Item>
                    <NavDropdown.Item href="/medicationList">Update/Delete Medication</NavDropdown.Item>
                </NavDropdown>
                <Nav.Link href="/medicalPlan">Create Medical Plan</Nav.Link>
                </Nav>
            <Nav className="justify-content-end">
                <Nav.Link href="/">Log out</Nav.Link>
            </Nav>
        </Navbar>
    )
}

export default MedicNavBar;