import React from "react"
import axios from "axios"
import MedicNavBar from "../MedicNavBar"
import UpdateMedication from "./UpdateMedication"
import ReactTable from "react-table";
import "react-table/react-table.css"
import {Form,FormGroup,Label,Input} from "reactstrap"

class ChoseMedication extends React.Component{
    constructor(props){
        super()
        console.log("pacientu")
        console.log(props.patient)
        this.state={
            medications:[],
            startTime:"",
            endTime:"",
            patient:props.patient,

        }

        this.handleChange=this.handleChange.bind(this)

    }

    componentDidMount(){
        this.getMedication()
    }

    getMedication(){
        axios.get("http://localhost:8090/medication/getAllMedications")
        .then(response=>{
            this.setState({
                medications:response.data
            })
            console.log(this.state.medications)
        })
    }

    handleChange(event){
        const {name,value}=event.target
        this.setState({
            [name]:value
        })
    }

    render(){
        return(
            <div>
                <Form className="plan-form">
                    <FormGroup check inline>
                        <Label className="add-label">Start Time</Label>
                        <Input className="input" 
                        type="text"
                        placeholder="Start time"
                        value={this.state.startTime}
                        name="startTime"
                        onChange={this.handleChange}/>
                        <Label className="add-label">End Time</Label>
                        <Input className="input" 
                        type="text"
                        placeholder="End time"
                        value={this.state.endTime}
                        name="endTime"
                        onChange={this.handleChange}/>
                    </FormGroup>
                </Form>

                <ReactTable className="-highlight -striped"

                    getTdProps={(state, rowInfo, column, instance) => {
                        return{
                            onClick:e=>{
                                const plan={
                                    startTime:this.state.startTime,
                                    endTime:this.state.endTime,
                                    medication:rowInfo.original,
                                    patient:this.state.patient,
                                }
                                console.log("aici este pacientul")
                                console.log(plan.patient)
                                if((plan.startTime!=="") && (plan.endTime!==""))
                                axios.post("http://localhost:8090/intake/saveIntake",plan)
                                .then(response=>
                                    this.setState({
                                        startTime:"",
                                        endTime:""
                                    }))
                                .catch(error=>error.response)
                            }
                        }
                    }}


                    data={this.state.medications}
                    columns={[
                    {
                        Header:"Id Medication",
                        accessor:"idMedication",
                    },
                        {
                        Header:"Name",
                        accessor:"name",
                    },
                    {
                        Header:"Dosage",
                        accessor:"dosage",
                    },
                    {
                        Header:"Side Effects",
                        accessor:"sideEffects",
                    },
                    ]}
              />
            </div>
        )
    }
}

export default ChoseMedication