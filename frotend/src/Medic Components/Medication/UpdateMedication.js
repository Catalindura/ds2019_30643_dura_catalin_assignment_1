import React from "react"
import MedicationNavBar from "../MedicNavBar"
import {Form,FormGroup,Input,Label,Button} from "reactstrap"
import axios from "axios"

class UpdateMedication extends React.Component{
    constructor(props){
        super()
        console.log(props)
        this.state={
            name:props.name,
            sideEffects:props.sideEffects,
            dosage:props.dosage,
            idMedication:props.idMedication,
        }

        this.handleChange=this.handleChange.bind(this)
        this.handleUpdate=this.handleUpdate.bind(this)
    }

    handleChange(event){
        const {name,value}=event.target
        this.setState({
            [name]:value
        })
    }

    handleUpdate(){
        const medication={
            name:this.state.name,
            sideEffects:this.state.sideEffects,
            dosage:this.state.dosage,
            idMedication:this.state.idMedication
        }
        axios.post("http://localhost:8090/medication/saveMedication",medication)
    }


    render(){
        return(
            <div>
               <Form className="add-form">

                     <h3>
                        <span className="user-message">
                        Medication
                        </span>
                    </h3>
                    <FormGroup>
                        <Label className="add-label">Medication Name</Label>
                        <Input className="input"
                         value={this.state.name}
                         type="text"
                         name="name"
                         onChange={this.handleChange}
                          />
                    </FormGroup>

                    <FormGroup>
                        <Label className="add-label">Side Effects</Label>
                        <Input className="input"
                         value={this.state.sideEffects}
                         type="text"
                         name="sideEffects"
                         onChange={this.handleChange}
                          />
                    </FormGroup>

                    <FormGroup>
                        <Label className="add-label">Dosage</Label>
                        <Input className="input"
                         value={this.state.dosage}
                         type="text"
                         name="dosage"
                         onChange={this.handleChange}
                          />
                    </FormGroup>

                    <Button className="btn-lg btn-block btn-succes" onClick={this.handleUpdate}>Update</Button>
                </Form>

            </div>
        )
    }
}

export default UpdateMedication