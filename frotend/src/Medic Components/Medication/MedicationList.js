import React from "react"
import axios from "axios"
import MedicNavBar from "../MedicNavBar"
import UpdateMedication from "./UpdateMedication"
import { throwStatement } from "@babel/types"

class MedicationList extends React.Component{
    constructor(){
        super()
        this.state={
            medications:[],
            update:"false",
            medicationToBeUpdated:{},
        }
        this.handleClick=this.handleClick.bind(this)
        this.handleUpdate=this.handleUpdate.bind(this)
    }

    componentDidMount(){
        this.getMedication()
    }

    getMedication(){
        axios.get("http://localhost:8090/medication/getAllMedications")
        .then(response=>{
            this.setState({
                medications:response.data
            })
            console.log(this.state.medications)
        })
    }

    handleClick(value){
        console.log(value)
        axios.post(`http://localhost:8090/medication/deleteMedication/${value}`)
        .then(()=>this.getMedication())
        .catch(error=>
            console.log(error.response))
    }

    handleUpdate(value){
        axios.get(`http://localhost:8090/medication/getMedicationById/${value}`)
        .then(response=>
            this.setState({
                medicationToBeUpdated:response.data,
                update:"true",
            })
        )

    }

    render(){
        return(
            <div>
                <MedicNavBar/>
                {this.state.update==="false" ?
                <table>
                    <tbody>
                        <tr>
                            <td>Medication Name</td>
                            <td>Side Effects</td>
                            <td>Dosage</td>
                            <td>Delete</td>
                            <td>Update</td>
                        </tr>
                        {this.state.medications.map(medication=>
                            <tr>
                                <td>{medication.name}</td>
                                <td>{medication.sideEffects}</td>
                                <td>{medication.dosage}</td>
                                <td><button className="button2" onClick={this.handleClick.bind(this,medication.idMedication)}>X</button></td>
                                <td><button className="button3" onClick={this.handleUpdate.bind(this,medication.idMedication)}>Update</button></td>
                            </tr>
                            )}
                    </tbody>
                </table>
                :
                <UpdateMedication
                name={this.state.medicationToBeUpdated.name}
                sideEffects={this.state.medicationToBeUpdated.sideEffects}
                dosage={this.state.medicationToBeUpdated.dosage}
                idMedication={this.state.medicationToBeUpdated.idMedication}/>}
            </div>
        )
    }
}

export default MedicationList