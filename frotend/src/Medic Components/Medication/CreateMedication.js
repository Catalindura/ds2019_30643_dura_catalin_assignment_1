import React from "react"
import axios from "axios"
import MedicNavBar from "../MedicNavBar"
import {Form,FormGroup,Input,Label,Button} from "reactstrap"

class CreateMedication extends React.Component{
    constructor(){
        super()
        this.state={
            name:"",
            sideEffects:"",
            dosage:"",
        }

        this.handleChange=this.handleChange.bind(this)
        this.handleClick=this.handleClick.bind(this)
    }

    handleChange(event){
        const{name,value}=event.target
        this.setState({
            [name]:value,
        }) 
    }

    handleClick(){
        console.log("salut")
        const medication={
            name:this.state.name,
            sideEffects:this.state.sideEffects,
            dosage:this.state.dosage,
        }

        axios.get(`http://localhost:8090/medication/findByName/${medication.name}`)
        .then(response=>{
            console.log(response.data)
            if(response.data==="yes"){
            console.log(medication)
            axios.post("http://localhost:8090/medication/saveMedication",medication)
            .then(response=>{
                console.log(response)
            })
        }

        })
    }



    render(){
        return(
            <div>
                <MedicNavBar/>

                <Form className="add-form">

                     <h3>
                        <span className="user-message">
                        Medication
                        </span>
                    </h3>
                    <FormGroup>
                        <Label className="add-label">Medication Name</Label>
                        <Input className="input"
                         value={this.state.name}
                         type="text"
                         name="name"
                         placeholder="Name"
                         onChange={this.handleChange}
                          />
                    </FormGroup>

                    <FormGroup>
                        <Label className="add-label">Side Effects</Label>
                        <Input className="input"
                         value={this.state.sideEffects}
                         type="text"
                         name="sideEffects"
                         placeholder="Side Effects"
                         onChange={this.handleChange}
                          />
                    </FormGroup>

                    <FormGroup>
                        <Label className="add-label">Dosage</Label>
                        <Input className="input"
                         value={this.state.dosage}
                         type="text"
                         name="dosage"
                         placeholder="Dosage"
                         onChange={this.handleChange}
                          />
                    </FormGroup>

                    <Button className="btn-lg btn-block btn-succes" onClick={this.handleClick}>Submit</Button>
                </Form>

            </div>
        )
    }
}

export default CreateMedication