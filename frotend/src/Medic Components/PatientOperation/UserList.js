import React from "react"
import axios from "axios"
import MedicNavBar from "../MedicNavBar"
import UpdateUser from "./UpdateUser"
import ChangeCaregiver from "../CaregiverOperation/ChangeCaregiver"

class UserList extends React.Component{
    constructor(){
        super()
        this.state={
            patients:[],
            update:"false",
            change:"false",
            userToBeUpdated:{},
            userToBeChanged:{},
        }
        this.getUsers=this.getUsers.bind(this)
        this.handleDelete=this.handleDelete.bind(this)
        this.handleCaregiver=this.handleCaregiver.bind(this)
    }
    componentDidMount(){
        this.getUsers()
    }

    getUsers(){
        axios.get("http://localhost:8090/patient/getAllPatients")
        .then(response=>{
            this.setState({
                patients:response.data
            })
            console.log(this.state.patients)
        })
    }

    handleDelete(value){
        console.log(value)
        axios.post(`http://localhost:8090/patient/deletePatient/${value}`)
        .then(()=>this.getUsers())
        .catch(error=>
            console.log(error))
    }

    handleUpdate(value){
        axios.get(`http://localhost:8090/patient/getPatientById/${value}`)
        .then(response=>{
            this.setState({
                userToBeUpdated:response.data,
                update:"true",
            })
            console.log(this.state.userToBeUpdated)
        }
            )
    }

    handleCaregiver(value){
       localStorage.setItem("idPatient",value)
       this.props.history.push("/changeCaregiver")

    }

    render(){
        return(
            <div>
                <MedicNavBar/>
                {this.state.update==="false" ?
                <table>
                    <tbody>
                        <tr>
                            <td>Username</td>
                            <td>Password</td>
                            <td>Name</td>
                            <td>Gender</td>
                            <td>Birth date</td>
                            <td>Address</td>
                            <td>Caregiver Name</td>
                            <td>Delete</td>
                            <td>Update</td>
                            <td>New Caregiver</td>
                        </tr>
                        {this.state.patients.map(user=>
                            <tr>
                                <td>{user.user.username}</td>
                                <td>{user.user.password}</td>
                                <td>{user.name}</td>
                                <td>{user.gender}</td>
                                <td>{user.birthDate}</td>
                                <td>{user.address}</td>
                                <td>{user.caregiver.name}</td>
                                <td><button className="button2" onClick={this.handleDelete.bind(this,user.idPatient)}>X</button></td>
                                <td><button className="button3" onClick={this.handleUpdate.bind(this,user.idPatient)}>Update</button></td>
                                <td><button className="button3" onClick={this.handleCaregiver.bind(this,user.idPatient)}>Change</button></td>
                            </tr>
                            )}
                    </tbody>
                </table>
                :
                <UpdateUser
                username={this.state.userToBeUpdated.user.username}
                type={this.state.userToBeUpdated.user.type}
                idAccount={this.state.userToBeUpdated.user.idUser}
                name={this.state.userToBeUpdated.name}
                password={this.state.userToBeUpdated.user.password}
                gender={this.state.userToBeUpdated.gender}
                birthDate={this.state.userToBeUpdated.birthDate}
                address={this.state.userToBeUpdated.address}
                idPatient={this.state.userToBeUpdated.idPatient}
                caregiver={this.state.userToBeUpdated.caregiver}/> 
                        }
            </div>
        )
    }
}

export default UserList