import React from 'react';
import './App.css';
import Login from "./Login Components/login.js"
import {BrowserRouter,Route,Switch} from "react-router-dom"
import MedicHomePage from "./Medic Components/MedicHomePage"
import CaregiverHomePage from "./CaregiverComponents/CaregiverHomePage"
import PatientHomePage from "./Patient Componets/PatientHomePage"
import CreateUser from "./Medic Components/CreateUser"
import UserList from "./Medic Components/PatientOperation/UserList"
import CaregiverList from "./Medic Components/CaregiverOperation/CaregiverList"
import CreateMedication from "./Medic Components/Medication/CreateMedication"
import MedicationList from "./Medic Components/Medication/MedicationList"
import MedicalPlan from "./Medic Components/MedicalPlan"
import ViewPlan from "./Patient Componets/ViewPlan"
import ViewPatients from "./CaregiverComponents/ViewPatients"
import { PrivateRouteMedic } from './PrivateRoutes/PrivateMedic'
import { PrivateRouteCaregiver } from './PrivateRoutes/PrivateCaregiver'
import { PrivateRoutePatient } from './PrivateRoutes/PrivatePatient'
import ChangeCaregiver from "./Medic Components/CaregiverOperation/ChangeCaregiver"

function App() {
  return (
    <div>
        <BrowserRouter>
        <Route exact path="/" component={Login}/>
        <PrivateRoutePatient exact path="/patient" component={PatientHomePage}/>
        <PrivateRouteMedic exact path="/medic" component={MedicHomePage}/>
        <PrivateRouteCaregiver exact path="/caregiver" component={CaregiverHomePage}/>
        <PrivateRouteMedic exact path="/createUser" component={CreateUser}/>
        <PrivateRouteMedic exact path="/patientList" component={UserList}/>
        <PrivateRouteMedic exact path="/caregiverList" component={CaregiverList}/>
        <PrivateRouteMedic exact path="/createMedication" component={CreateMedication}/>
        <PrivateRouteMedic exact path="/medicationList" component={MedicationList}/>
        <PrivateRouteMedic exact path="/medicalPlan" component={MedicalPlan}/>
        <PrivateRoutePatient exact path="/patientPlan" component={ViewPlan}/>
        <PrivateRouteCaregiver exact path="/viewPatients" component={ViewPatients}/>
        <PrivateRouteMedic exact path="/changeCaregiver" component={ChangeCaregiver}/>
         </BrowserRouter> 
    </div>
  );
}

export default App;
