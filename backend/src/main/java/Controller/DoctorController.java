package Controller;

import Dto.CaregiverDto;
import Dto.DoctorDto;
import Dto.PatientDto;
import Entity.Caregiver;
import Service.CaregiverService;
import Service.DoctorService;
import Service.PatientService;
import Service.UserService;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import java.util.List;

@RestController
@CrossOrigin(maxAge=3600)
@RequestMapping("/medic")
public class DoctorController {

    @Autowired
    PatientService patientService;
    @Autowired
    CaregiverService caregiverService;
    @Autowired
    DoctorService doctorService;
    @Autowired
    UserService userService;

    @RequestMapping(value="/getDoctorByUserId/{id}",method = RequestMethod.GET)
    public DoctorDto getDoctorByUserId(@PathVariable int id){
        return doctorService.getDoctorByIdUser(id);
    }

}
