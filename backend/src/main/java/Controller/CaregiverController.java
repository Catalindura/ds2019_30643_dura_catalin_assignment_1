package Controller;

import Dto.CaregiverDto;
import Dto.PatientDto;
import Service.CaregiverService;
import Service.PatientService;
import Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(maxAge=3600)
@RequestMapping("/caregiver")
public class CaregiverController {
    @Autowired
    CaregiverService caregiverService;
    @Autowired
    PatientService patientService;
    @Autowired
    UserService userService;

    @RequestMapping(value="/getCaregiverList/{id}",method= RequestMethod.GET)
    public List<PatientDto> getCaregiverPatients(@PathVariable int id){
        return patientService.getAllPacientsByCaregiverId(caregiverService.getCaregiverIdByUserId(id));
    }

    @RequestMapping(value="/getAllCaregivers",method=RequestMethod.GET)
    public List<CaregiverDto> getAllCaregivers(){
        return caregiverService.getAllCaregivers();
    }

    @Transactional
    @RequestMapping(value="/deleteCaregiver/{id}",method=RequestMethod.POST)
    public void deleteCaregiver(@PathVariable int id){
        List<PatientDto> caregiverPatients=patientService.getAllPacientsByCaregiverId(id);
        CaregiverDto newCaregiver=caregiverService.getFirstCaregiverWithIdDifferentFrom(id);
        System.out.println(newCaregiver.getName());
        for(PatientDto aux:caregiverPatients) {
            aux.setCaregiver(newCaregiver.caregiverDtoToCaregiver());
            patientService.savePatient(aux);
        }
        userService.deleteUserById(caregiverService.getCaregiverById(id).getUser().getIdUser());
        caregiverService.deleteCaregiverById(id);
    }

    @RequestMapping(value="/getFirstCaregiver",method = RequestMethod.GET)
    public CaregiverDto getFirstCaregiver(){
        return caregiverService.getFirstCaregiver();
    }

    @RequestMapping(value="/getCaregiverById/{id}",method = RequestMethod.GET)
    public CaregiverDto getCaregiverById(@PathVariable int id){
        return caregiverService.getCaregiverById(id);
    }

    @RequestMapping(value="/saveCaregiver",method = RequestMethod.POST)
    public void saveCaregiver(@RequestBody CaregiverDto caregiverDto){
        caregiverService.saveCaregiver(caregiverDto);
    }
}
