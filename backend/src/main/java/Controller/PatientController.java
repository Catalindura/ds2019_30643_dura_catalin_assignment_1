package Controller;

import Dto.PatientDto;
import Service.IntakeService;
import Service.PatientService;
import Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/patient")
public class PatientController {
    @Autowired
    PatientService patientService;
    @Autowired
    IntakeService intakeService;
    @Autowired
    UserService userService;

    @RequestMapping(value="/getPatientByUserId/{id}",method= RequestMethod.GET)
    public PatientDto getPatientByUserId(@PathVariable int id){
        return patientService.getPatientByUserId(id);
    }

    @Transactional
    @RequestMapping(value="/deletePatient/{id}",method=RequestMethod.POST)
    public void deletePatient(@PathVariable int id){

        intakeService.deleteAllByPatient(id);
        userService.deleteUserById(patientService.getPatientById(id).getUser().getIdUser());
        patientService.deletePatient(id);

    }

    @RequestMapping(value="/getPatientById/{id}",method = RequestMethod.GET)
    public PatientDto getPatientById(@PathVariable int id){
        return patientService.getPatientById(id);
    }

    @RequestMapping(value="/savePatient",method = RequestMethod.POST)
    public void savePacient(@RequestBody PatientDto pacientDto){
        patientService.savePatient(pacientDto);
    }


    @RequestMapping(value="/getAllPatients",method=RequestMethod.GET)
    public List<PatientDto> getAllPatients(){
        return patientService.getAllPacients();
    }
}
