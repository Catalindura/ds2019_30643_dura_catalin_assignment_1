package Controller;

import Dto.IntakeDto;
import Service.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(maxAge=3600)
@RequestMapping("/intake")
public class IntakeController {
    @Autowired
    IntakeService intakeService;

    @RequestMapping(value="/saveIntake",method= RequestMethod.POST)
    public void saveIntake(@RequestBody IntakeDto intakeDto){
        intakeService.saveIntake(intakeDto);
    }

    @RequestMapping(value="/getPatientIntake/{id}",method=RequestMethod.GET)
    public List<IntakeDto> getPatientIntake(@PathVariable int id){
        return intakeService.findPatientIntake(id);
    }
}
