package Controller;

import Dto.UserDto;
import Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;



    @RequestMapping(value="/getUser/{username}",method=RequestMethod.GET)

    public UserDto getUserByUsername(@PathVariable String username){
        return userService.getUserByUsername(username);
    }

    @RequestMapping(value="/checkUser/{username}",method = RequestMethod.GET)
    public String checkUser(@PathVariable String username){
        return userService.checkUser(username);
    }

    @RequestMapping(value="/saveUser",method = RequestMethod.POST)
    public int saveUser(@RequestBody UserDto userDto){
        return userService.saveUser(userDto);
    }
}
