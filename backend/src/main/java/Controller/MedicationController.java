package Controller;

import Dto.MedicationDto;
import Entity.Medication;
import Service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(maxAge=3600)
@RequestMapping("/medication")
public class MedicationController {
    @Autowired
    MedicationService medicationService;

    @RequestMapping(value="/saveMedication",method= RequestMethod.POST)
    public void saveMedication(@RequestBody MedicationDto medicationDto){
        medicationService.saveMedication(medicationDto);
    }

    @RequestMapping(value="/findByName/{name}",method = RequestMethod.GET)
    public String findByName(@PathVariable String name){
        return medicationService.findByName(name);
    }

    @RequestMapping(value="/getAllMedications",method=RequestMethod.GET)
    public List<MedicationDto> getAllMedications() {
        return medicationService.getAllMedication();
    }

    @Transactional
    @RequestMapping(value="/deleteMedication/{id}",method = RequestMethod.POST)
    public void deleteMedication(@PathVariable int id){
        medicationService.deleteMedicationById(id);
    }

    @RequestMapping(value="/getMedicationById/{id}",method=RequestMethod.GET)
    public MedicationDto getMedicationById(@PathVariable int id){
        return medicationService.getMedicationById(id);
    }
}
