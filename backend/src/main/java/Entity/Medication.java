package Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="medication")
public class Medication {
    @Id
    @Column(name="idmedication",nullable=false,unique=true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMedication;

    @Column(name="name")
    private String name;

    @Column(name="sideeffects")
    private String sideEffects;

    @Column(name="dosage")
    private String dosage;

    @JsonIgnore
    @OneToMany(mappedBy = "medication",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Intake> intakes;

    public Medication(){}

    public Medication(Integer idMedication,String name, String sideEffects, String dosage) {
        this.idMedication=idMedication;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public List<Intake> getIntakes() {
        return intakes;
    }

    public void setIntakes(List<Intake> intakes) {
        this.intakes = intakes;
    }

    public Integer getIdMedication() {
        return idMedication;
    }

    public void setIdMedication(Integer idMedication) {
        this.idMedication = idMedication;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEfects) {
        this.sideEffects = sideEfects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
