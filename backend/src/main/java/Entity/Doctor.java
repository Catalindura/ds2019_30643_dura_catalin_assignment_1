package Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="doctor")
public class Doctor {
    @Id
    @Column(name="iddoctor",nullable=false,unique=true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDoctor;

    @OneToOne
    @JoinColumn(name="iduser")
    private User user;

    @Column(name="name")
    private String name;

    @Column(name="email")
    private String email;

    @JsonIgnore
    @OneToMany(mappedBy = "doctor",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Caregiver> caregivers;

    public Doctor(){}

    public Doctor(Integer idDoctor,User user, String name, String email) {
        this.idDoctor=idDoctor;
        this.user = user;
        this.name = name;
        this.email = email;
    }

    public List<Caregiver> getCaregivers() {
        return caregivers;
    }

    public void setCaregivers(List<Caregiver> caregivers) {
        this.caregivers = caregivers;
    }

    public Integer getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Integer idDoctor) {
        this.idDoctor = idDoctor;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
