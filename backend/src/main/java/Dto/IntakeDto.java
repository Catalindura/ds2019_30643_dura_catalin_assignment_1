package Dto;

import Entity.Intake;
import Entity.Medication;
import Entity.Patient;

public class IntakeDto {
    private Integer idIntake;
    private Patient patient;
    private Medication medication;
    private String startTime;
    private String endTime;

    public IntakeDto(Integer idIntake, Patient patient, Medication medication, String startTime, String endTime) {
        this.idIntake = idIntake;
        this.patient = patient;
        this.medication = medication;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getIdIntake() {
        return idIntake;
    }

    public void setIdIntake(Integer idIntake) {
        this.idIntake = idIntake;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Intake intakeDtoToIntake(){
        return new Intake(this.idIntake,
                this.patient,
                this.medication,
                this.startTime,
                this.endTime);
    }
}
