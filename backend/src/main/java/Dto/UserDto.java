package Dto;

import Entity.User;

public class UserDto {
    private Integer idUser;
    private String username;
    private String password;
    private String type;

    public UserDto(Integer idUser, String username, String password, String type) {
        this.idUser = idUser;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public UserDto(){}

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User DtoToUser(){
        return new User(this.idUser,this.username,this.password,this.type);
    }
}
