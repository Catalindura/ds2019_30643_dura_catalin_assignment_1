package Dto;

import Entity.Medication;

public class MedicationDto {
    private Integer idMedication;
    private String name;
    private String sideEffects;
    private String dosage;

    public MedicationDto(Integer idMedication, String name, String sideEffects, String dosage) {
        this.idMedication = idMedication;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Integer getIdMedication() {
        return idMedication;
    }

    public void setIdMedication(Integer idMedication) {
        this.idMedication = idMedication;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public Medication medicationDtoToMedication(){
        return new Medication(this.idMedication,
                this.name,
                this.sideEffects,
                this.dosage);
    }
}
