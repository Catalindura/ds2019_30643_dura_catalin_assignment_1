package Dto;

import Entity.Caregiver;
import Entity.Patient;
import Entity.User;

public class PatientDto {
    private Integer idPatient;
    private String name;
    private String address;
    private String birthDate;
    private User user;
    private Caregiver caregiver;
    private String gender;

    public PatientDto(Integer idPatient, String name, String address, String birthDate, User user, Caregiver caregiver, String gender) {
        this.idPatient = idPatient;
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
        this.user = user;
        this.caregiver = caregiver;
        this.gender = gender;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Patient patientDtoToPatient(){
        return new Patient(this.idPatient,this.user,this.caregiver,this.name,this.address,this.gender,this.birthDate);
    }
}
