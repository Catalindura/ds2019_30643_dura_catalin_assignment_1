package Dto;

import Entity.Doctor;
import Entity.User;

public class DoctorDto {
    private Integer idDoctor;
    private User user;
    private String name;
    private String email;

    public DoctorDto(Integer idDoctor, User user, String name, String email) {
        this.idDoctor = idDoctor;
        this.user = user;
        this.name = name;
        this.email = email;
    }

    public Integer getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Integer idDoctor) {
        this.idDoctor = idDoctor;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Doctor DoctorDtoToDoctor(){
        return new Doctor(this.idDoctor,this.user,this.name,this.email);
    }
}
