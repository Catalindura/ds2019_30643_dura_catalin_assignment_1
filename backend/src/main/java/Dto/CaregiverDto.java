package Dto;

import Entity.Caregiver;
import Entity.Doctor;
import Entity.User;

public class CaregiverDto {
    private Integer idCaregiver;
    private String name;
    private String address;
    private String birthDate;
    private User user;
    private Doctor doctor;
    private String gender;

    public CaregiverDto(){}

    public CaregiverDto(Integer idCaregiver, String name, String address, String birthDate, User user, Doctor docotor, String gender) {
        this.idCaregiver=idCaregiver;
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
        this.user = user;
        this.doctor = docotor;
        this.gender = gender;
    }

    public CaregiverDto( String name, String address, String birthDate, User user, Doctor doctor, String gender) {
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
        this.user = user;
        this.doctor = doctor;
        this.gender = gender;
    }

    public Integer getIdCaregiver() {
        return idCaregiver;
    }

    public void setIdCaregiver(Integer idCaregiver) {
        this.idCaregiver = idCaregiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Caregiver caregiverDtoToCaregiver(){
        return new Caregiver(this.idCaregiver,this.user,this.doctor,this.name,this.address,this.gender,this.birthDate);
    }
}
