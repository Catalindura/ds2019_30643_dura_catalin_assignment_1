package Service;

import Dto.UserDto;
import Entity.User;
import Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public UserDto getUserByUsername(String username){
        User user=userRepository.getByUsername(username);
        return new UserDto(user.getIdUser(),user.getUsername(),user.getPassword(),user.getType());
    }

    public String checkUser(String username){
        if(userRepository.getByUsername(username)==null)
            return "yes";
        else return "no";
    }

    public int saveUser(UserDto userDto){
        User user=new User(userDto.getIdUser(),
                userDto.getUsername(),
                userDto.getPassword(),
                userDto.getType());
        userRepository.save(user);
        return user.getIdUser();
    }

    public void deleteUserById(int id){
        userRepository.deleteByIdUser(id);
    }
}
