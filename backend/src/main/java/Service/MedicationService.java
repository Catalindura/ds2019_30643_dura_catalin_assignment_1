package Service;

import Dto.MedicationDto;
import Entity.Medication;
import Repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MedicationService {
    @Autowired
    MedicationRepository medicationRepository;

    public void saveMedication(MedicationDto medicationDto){
        medicationRepository.save(medicationDto.medicationDtoToMedication());
    }

    public List<MedicationDto> getAllMedication(){
        List<Medication> medications=medicationRepository.findAll();
        List<MedicationDto> medicationsDto=new ArrayList<MedicationDto>();
        for(Medication aux:medications){
            medicationsDto.add(new MedicationDto(aux.getIdMedication(),
                    aux.getName(),
                    aux.getSideEffects(),
                    aux.getDosage()));
        }
        return medicationsDto;
    }

    public void deleteMedicationById(int id){
        medicationRepository.deleteByIdMedication(id);
    }

    public MedicationDto getMedicationById(int id){
        Medication medication=medicationRepository.getMedicationByIdMedication(id);
        return new MedicationDto(medication.getIdMedication(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage());
    }

    public String findByName(String name){
        if(medicationRepository.findByName(name)==null)
            return "yes";
        else return "no";
    }
}
