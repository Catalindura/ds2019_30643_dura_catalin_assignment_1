package Service;

import Dto.IntakeDto;
import Dto.PatientDto;
import Entity.Intake;
import Repository.IntakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IntakeService {
    @Autowired
    IntakeRepository intakeRepository;

    public void saveIntake(IntakeDto intakeDto){
        intakeRepository.save(intakeDto.intakeDtoToIntake());
    }

    public List<IntakeDto> findPatientIntake(int id){
        List<Intake> plan=intakeRepository.findAllByPatientIdPatient(id);
        List<IntakeDto> planDto=new ArrayList<IntakeDto>();
        for(Intake aux:plan){
            planDto.add(new IntakeDto(aux.getIdIntake(),
                    aux.getPatient(),
                    aux.getMedication(),
                    aux.getStartTime(),
                    aux.getEndTime()));
        }
        return planDto;
    }

    public void deleteAllByPatient(int id){
        intakeRepository.deleteAllByPatientIdPatient(id);
    }
}
