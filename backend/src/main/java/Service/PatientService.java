package Service;

import Dto.PatientDto;
import Entity.Patient;
import Repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientService {
    @Autowired
    PatientRepository patientRepository;

    public PatientDto getPatientById(int id){
        Patient patient=patientRepository.getByIdPatient(id);
        return new PatientDto(patient.getIdPatient(),
                patient.getName(),
                patient.getAddress(),
                patient.getBirthDate(),
                patient.getUser(),
                patient.getCaregiver(),
                patient.getGender());
    }

    public void savePatient(PatientDto patientDto){
        patientRepository.save(new Patient(
           patientDto.getIdPatient(),
           patientDto.getUser(),
           patientDto.getCaregiver(),
           patientDto.getName(),
           patientDto.getAddress(),
           patientDto.getGender(),
           patientDto.getBirthDate()
        ));
    }

    public List<PatientDto> getAllPacients(){
        List<Patient> pacients=patientRepository.findAll();
        List<PatientDto> pacientsDto=new ArrayList<PatientDto>();
        for (Patient aux:pacients){
            pacientsDto.add(new PatientDto(aux.getIdPatient(),
                    aux.getName(),
                    aux.getAddress(),
                    aux.getBirthDate(),
                    aux.getUser(),
                    aux.getCaregiver(),
                    aux.getGender()));
        }
        return pacientsDto;
    }

    public List<PatientDto> getAllPacientsByCaregiverId(int id){
        List<Patient> pacients=patientRepository.getPatientByCaregiverIdCaregiver(id);
        List<PatientDto> pacientsDto=new ArrayList<PatientDto>();
        for (Patient aux:pacients){
            pacientsDto.add(new PatientDto(aux.getIdPatient(),
                    aux.getName(),
                    aux.getAddress(),
                    aux.getBirthDate(),
                    aux.getUser(),
                    aux.getCaregiver(),
                    aux.getGender()));
        }
        return pacientsDto;
    }

    public void deletePatient(int id){
        patientRepository.deleteByIdPatient(id);
    }

    public PatientDto getPatientByUserId(int id){
        Patient patient=patientRepository.getPatientByUserIdUser(id);
        return new PatientDto(patient.getIdPatient(),
                patient.getName(),
                patient.getAddress(),
                patient.getBirthDate(),
                patient.getUser(),
                patient.getCaregiver(),
                patient.getGender());
    }
}
