package Service;

import Dto.CaregiverDto;
import Dto.PatientDto;
import Entity.Caregiver;
import Repository.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CaregiverService {
    @Autowired
    CaregiverRepository caregiverRepository;

    public CaregiverDto getCaregiverById(int id){
        Caregiver caregiver=caregiverRepository.getByIdCaregiver(id);
        return new CaregiverDto(caregiver.getIdCaregiver(),
                caregiver.getName(),
                caregiver.getAddress(),
                caregiver.getBirthDate(),
                caregiver.getUser(),
                caregiver.getDoctor(),
                caregiver.getGender());
    }

    public void saveCaregiver(CaregiverDto caregiverDto){
        caregiverRepository.save(new Caregiver(caregiverDto.getIdCaregiver(),
                caregiverDto.getUser(),
                caregiverDto.getDoctor(),
                caregiverDto.getName(),
                caregiverDto.getAddress(),
                caregiverDto.getGender(),
                caregiverDto.getBirthDate()));
    }

    public CaregiverDto getFirstCaregiver(){
        Caregiver caregiver=caregiverRepository.findCaregiverByIdCaregiverGreaterThan(0).get(0);
        return new CaregiverDto(caregiver.getIdCaregiver(),
                caregiver.getName(),
                caregiver.getAddress(),
                caregiver.getBirthDate(),
                caregiver.getUser(),
                caregiver.getDoctor(),
                caregiver.getGender()
        );
    }

    public CaregiverDto getFirstCaregiverWithIdDifferentFrom(int id){
        Caregiver caregiver=caregiverRepository.findFirstByIdCaregiverIsNotLike(id);
        return new CaregiverDto(caregiver.getIdCaregiver(),
                caregiver.getName(),
                caregiver.getAddress(),
                caregiver.getBirthDate(),
                caregiver.getUser(),
                caregiver.getDoctor(),
                caregiver.getGender()
        );
    }

    public List<CaregiverDto> getAllCaregivers(){
        List<Caregiver> caregivers=caregiverRepository.findAll();
        List<CaregiverDto> caregiversDto=new ArrayList<CaregiverDto>();
        for (Caregiver aux:caregivers){
           caregiversDto.add(new CaregiverDto(aux.getIdCaregiver(),
                   aux.getName(),
                   aux.getAddress(),
                   aux.getBirthDate(),
                   aux.getUser(),
                   aux.getDoctor(),
                   aux.getGender()));
        }
        return caregiversDto;
    }

    public void deleteCaregiverById(int id){
        caregiverRepository.deleteByIdCaregiver(id);
    }

    public int getCaregiverIdByUserId(int id){
        return caregiverRepository.findCaregiverByUserIdUser(id).getIdCaregiver();
    }
}
