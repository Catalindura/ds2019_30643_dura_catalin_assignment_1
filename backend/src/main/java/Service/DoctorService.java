package Service;

import Dto.DoctorDto;
import Entity.Doctor;
import Repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DoctorService {
    @Autowired
    DoctorRepository doctorRepository;

    public DoctorDto getDoctorById(int id){
        Doctor doctor=doctorRepository.getByIdDoctor(id);
        return new DoctorDto(doctor.getIdDoctor(),doctor.getUser(),doctor.getName(),doctor.getEmail());
    }

    public DoctorDto getDoctorByIdUser(int id){
        Doctor doctor=doctorRepository.getByUserIdUser(id);
        return new DoctorDto(doctor.getIdDoctor(),doctor.getUser(),doctor.getName(),doctor.getEmail());
    }
}
