package Repository;

import Entity.Intake;
import Entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IntakeRepository extends JpaRepository<Intake,Integer> {
    public Intake save(Intake intake);
    public List<Intake> findAllByPatientIdPatient(int id);
    public void deleteAllByPatientIdPatient(int id);
}
