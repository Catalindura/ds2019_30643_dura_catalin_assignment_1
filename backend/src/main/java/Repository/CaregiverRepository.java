package Repository;

import Entity.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CaregiverRepository extends JpaRepository<Caregiver,Integer> {
    public Caregiver save(Caregiver caregiver);
    public Caregiver getByIdCaregiver(int id);
    public List<Caregiver> findAll();
    public List<Caregiver> findCaregiverByIdCaregiverGreaterThan(int number);
    public Caregiver findFirstByIdCaregiverIsNotLike(int number);
    public void deleteByIdCaregiver(int id);
    public Caregiver findCaregiverByUserIdUser(int id);
 }
