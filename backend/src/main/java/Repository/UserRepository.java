package Repository;

import Dto.UserDto;
import Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
    public User save(User user);
    public User getByUsername(String username);
    public void deleteByIdUser(int id);
}
