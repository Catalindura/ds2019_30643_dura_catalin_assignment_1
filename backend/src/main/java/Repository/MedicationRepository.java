package Repository;

import Entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MedicationRepository extends JpaRepository<Medication,Integer> {
    public Medication save(Medication medication);
    public List<Medication> findAll();
    public Medication getMedicationByIdMedication(int id);
    public void deleteByIdMedication(int id);
    public Medication findByName(String name);
}
