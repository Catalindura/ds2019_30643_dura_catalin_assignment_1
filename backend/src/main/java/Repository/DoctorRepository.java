package Repository;

import Entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository extends JpaRepository<Doctor,Integer> {
    public Doctor getByIdDoctor(int id);
    public Doctor getByUserIdUser(int id);
}
