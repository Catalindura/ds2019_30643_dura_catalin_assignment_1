package Repository;

import Entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PatientRepository extends JpaRepository<Patient,Integer> {
    public Patient save(Patient patient);
    public Patient getByIdPatient(int id);
    public List<Patient> findAll();
    public void deleteByIdPatient(int id);
    public List<Patient> getPatientByCaregiverIdCaregiver(int id);
    public Patient getPatientByUserIdUser(int id);
}
